#include "gtest/gtest.h"
#include "../src/debug_cout.h"
#include "../src/matrix.h"
#include "../src/ray.h"
#include "../src/sphere.h"
#include "../src/vec.h"
#include <algorithm>
#include <cassert>
#include <iostream>
#include <ostream>
#if 0
template <typename T>
std::ostream &operator<<(std::ostream &os, const Vec3<T> &v) {
  return os << '(' << v._v[0] << ',' << v._v[1] << ',' << v._v[2] << ')';
}

template <typename T>
std::ostream &operator<<(std::ostream &os, const Matrix33<T> &m) {
  return os << '(' << Vec3<T>{m._m[0]} << ',' << Vec3<T>{m._m[1]} << ','
            << Vec3<T>{m._m[2]} << ')';
}

int main2() {
  auto v1 = Vec3d{1, 2, 3};
  auto up = Vec3d{0, 1, 0};
  auto right = Vec3d{1, 0, 0};
  auto out = Vec3d{0, 0, 1};

  assert(right.cross(up) == out);
  assert(up.dot(up) == 1.0);
  assert(up.dot(right) == 0.0);

  auto s1 = SphereD{{0, 0, 0}, 1};
  auto r1 = RayD{{0, 0, -8}, {0, 0, 1}};

  auto d1 = std::vector<double>{7, 9};
  // auto i1 = s1.intersect(r1);
  // std::sort(i1.begin(), i1.end());
  // assert(d1 == i1);

  auto m1 = Matrix33d::rotate_x(pi_d / 2);
  std::cout << m1 << "\n";
  auto v2 = m1 * up;
  std::cout << v2 << " " << out << "\n";
  assert(v2.almost(out, 0.0001));
}
#endif

TEST(TestCross, AsExpected)
{
    auto v1 = Vec3d{1, 2, 3};
    auto up = Vec3d{0, 1, 0};
    auto right = Vec3d{1, 0, 0};
    auto out = Vec3d{0, 0, 1};

    EXPECT_EQ(right.cross(up), out);
    EXPECT_EQ(up.dot(up), 1.0);
    EXPECT_EQ(up.dot(right), 0.0);
    // EXPECT_EQ(1,2);
}

TEST(TestTest, Fails)
{
    EXPECT_EQ(1, 1);
}

int main(int argc, char** argv)
{
    ::testing::InitGoogleTest(&argc, argv);
    return RUN_ALL_TESTS();
}