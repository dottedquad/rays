#include "../src/sphere.h"
#include "../src/debug_cout.h"
#include "gtest/gtest.h"

template <typename T>
::testing::AssertionResult Near(const T& a, const T& b, float delta) {
  if (a.almost(b, delta))
    return ::testing::AssertionSuccess();
  else
    return ::testing::AssertionFailure() << a << " ~!=  " << b;
}

TEST(TestSphere, AsExpected)
{
    const auto scale = Matrix34d::scale({.5, .5, .5});
    const auto translate = Matrix34d::translate({4, 0, 0});
    const auto transform = translate * scale;

    const auto inv_scale = Matrix34d::scale({2, 2, 2});
    const auto inv_translate = Matrix34d::translate({-4, 0, 0});
    const auto inv_transform = inv_scale * inv_translate;

    const auto sphere = Sphere{transform, inv_transform};
    const auto ray = Ray<double>{{1,0,0},{1,0,0}};

    const auto intersections = sphere.intersect(ray);

    ASSERT_EQ(intersections.size(), 2);
    const auto pt0 = ray.destination(intersections[0].first);
    const auto pt1 = ray.destination(intersections[1].first);
    EXPECT_EQ(pt0, Vec3d(3.5,0,0));
    EXPECT_EQ(pt1, Vec3d(4.5,0,0));
}

TEST(TestStretchedSphere, AsExpected)
{
    const auto scale = Matrix34d::scale({.5, 2, 1});
    const auto translate = Matrix34d::translate({5, 0, 0});
    const auto transform = translate * scale;

    const auto inv_scale = Matrix34d::scale({2, .5, 1});
    const auto inv_translate = Matrix34d::translate({-5, 0, 0});
    const auto inv_transform = inv_scale * inv_translate;

    const auto sphere = Sphere{transform, inv_transform};
    const auto ray = Ray<double>{{1,0,0},Vec3d{4,2,0}.normalize()};

    const auto intersections = sphere.intersect(ray);

    ASSERT_EQ(intersections.size(), 2);
    const auto pt0 = ray.destination(intersections[0].first);
    const auto pt1 = ray.destination(intersections[1].first);
    EXPECT_TRUE(Near(pt0, Vec3d(4.87692,1.93846,0), 0.00001));
    EXPECT_TRUE(Near(pt1, Vec3d(5,2,0), 0.00001));
    // EXPECT_EQ(1,2);
}

TEST(TestStretchedSphere2, AsExpected)
{
    const auto scale = Matrix34d::scale({.005, 2, 1});
    const auto translate = Matrix34d::translate({5, 0, 0});
    const auto transform = translate * scale;

    const auto inv_scale = Matrix34d::scale({200, .5, 1});
    const auto inv_translate = Matrix34d::translate({-5, 0, 0});
    const auto inv_transform = inv_scale * inv_translate;

    const auto sphere = Sphere{transform, inv_transform};
    const auto ray = Ray<double>{{1,0,0},Vec3d{4,1,0}.normalize()};

    const auto intersections = sphere.intersect(ray);

    ASSERT_EQ(intersections.size(), 2);
    const auto pt0 = ray.destination(intersections[0].first);
    const auto pt1 = ray.destination(intersections[1].first);
    EXPECT_TRUE(Near(pt0, Vec3d(4.99566,0.998917,0), 0.00001));
    EXPECT_TRUE(Near(pt1, Vec3d(5.00432,1.001082,0), 0.00001));
    // EXPECT_EQ(1,2);
}

TEST(TestRotatedSphere, AsExpected)
{
    const auto rotate = Matrix34d::rotate({0,0,1}, pi_d/4);
    const auto translate = Matrix34d::translate({5, 0, 0});
    const auto transform = translate * rotate;

    const auto inv_rotate = Matrix34d::rotate({0,0,1}, -pi_d/4);
    const auto inv_translate = Matrix34d::translate({-5, 0, 0});
    const auto inv_transform = inv_rotate * inv_translate;

    const auto sphere = Sphere{transform, inv_transform};
    const auto ray1 = Ray<double>{{1,0,0},Vec3d{4,0,0}.normalize()};
    const auto ray2 = Ray<double>{{1,0,0},Vec3d{4,1,0}.normalize()};
    const auto ray3 = Ray<double>{{1,0,0},Vec3d{4,-0.5,0}.normalize()};
    const auto ray4 = Ray<double>{{1,0,0},Vec3d{4,2,0}.normalize()};

    const auto intersections1 = sphere.intersect(ray1);
    const auto intersections2 = sphere.intersect(ray2);
    const auto intersections3 = sphere.intersect(ray3);
    const auto intersections4 = sphere.intersect(ray4);

    ASSERT_EQ(intersections1.size(), 2);
    const auto pt10 = ray1.destination(intersections1[0].first);
    const auto pt11 = ray1.destination(intersections1[1].first);
    EXPECT_TRUE(Near(pt10, Vec3d(4,0,0), 0.00001));
    EXPECT_TRUE(Near(pt11, Vec3d(6,0,0), 0.00001));

    ASSERT_EQ(intersections2.size(), 2);
    const auto pt20 = ray2.destination(intersections2[0].first);
    const auto pt21 = ray2.destination(intersections2[1].first);
    EXPECT_TRUE(Near(pt20, Vec3d(4.52941,0.882353,0), 0.00001));
    EXPECT_TRUE(Near(pt21, Vec3d(5,1,0), 0.00001));

    ASSERT_EQ(intersections3.size(), 2);
    const auto pt30 = ray3.destination(intersections3[0].first);
    const auto pt31 = ray3.destination(intersections3[1].first);
    EXPECT_TRUE(Near(pt30, Vec3d(4.07692,-0.384615,0), 0.00001));
    EXPECT_TRUE(Near(pt31, Vec3d(5.8,-0.6,0), 0.00001));
    
    EXPECT_EQ(intersections4.size(), 0);
}

TEST(TestStretchedSphere3, AsExpected)
{
    const auto scale_factor = Vec3d{.00005, 2, 1};
    const auto rotation_axis = Vec3d{0,0,1};
    const auto rotation_angle = pi_d/4;
    const auto translate_amount = Vec3d{5,0,0};
    const auto scale = Matrix34d::scale(scale_factor);
    const auto rotate = Matrix34d::rotate(rotation_axis, rotation_angle);
    const auto translate = Matrix34d::translate(translate_amount);
    const auto transform = translate * rotate * scale;

    const auto inv_scale = Matrix34d::scale(1.0/scale_factor);
    const auto inv_rotate = Matrix34d::rotate(rotation_axis, -rotation_angle);
    const auto inv_translate = Matrix34d::translate(-translate_amount);
    const auto inv_transform = inv_scale * inv_rotate * inv_translate;

    const auto sphere = Sphere{transform, inv_transform};
    const auto ray = Ray<double>{{1,0,0},Vec3d{4,0,0}.normalize()};

    auto pt = Vec3d{1,0,0};

    const auto intersections = sphere.intersect(ray);

    ASSERT_EQ(intersections.size(), 2);
    const auto pt0 = ray.destination(intersections[0].first);
    const auto pt1 = ray.destination(intersections[1].first);
    EXPECT_TRUE(Near(pt0, Vec3d(4.99993,0,0), 0.00001));
    EXPECT_TRUE(Near(pt1, Vec3d(5.00007,0,0), 0.00001));
    EXPECT_TRUE(Near(intersections[0].second, Vec3d(-.707107,-.707107,0), 0.00001));
    EXPECT_TRUE(Near(intersections[1].second, Vec3d(.707107,.707107,0), 0.00001));
}
