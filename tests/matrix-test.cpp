#include "../src/matrix.h"
#include "../src/debug_cout.h"
#include "gtest/gtest.h"

TEST(TestRandomRotate, AsExpected)
{
    const auto p1 = Vec3d{1, 2, 3};
    const auto axis1 = Vec3d{4,-5,.2}.normalize();
    const auto angle = pi_d/4;
    const auto r1 = Matrix33d::rotate(axis1, angle);

    const auto rotated_point = r1 * p1;

    EXPECT_TRUE(rotated_point.almost({-1.14686,0.30445,3.54852}, 0.0001));
}

TEST(TestRotate, AsExpected)
{
    const auto p1 = Vec3d{1, 2, 3};
    const auto up = Vec3d{0, 1, 0};
    const auto angle = pi_d/2;
    const auto r1 = Matrix33d::rotate(up, angle);

    const auto rotated_point = r1 * p1;

    EXPECT_TRUE(rotated_point.almost({3,2,-1}, 0.0001));
}

TEST(TestScale, AsExpected)
{
    const auto p1 = Vec3d{1, 2, 3};
    const auto s1 = Matrix33d::scale({.5,.5,.5});

    const auto scaled_point = s1 * p1;

    EXPECT_TRUE(scaled_point.almost({.5,1,1.5}, 0.0001));
}

TEST(TestTranslate, AsExpected)
{
    const auto p1 = Vec3d{1, 2, 3};
    const auto t1 = Matrix34d::translate({5,6,7});

    const auto translated_point = t1 * p1;

    EXPECT_TRUE(translated_point.almost({6,8,10}, 0.0001));
}

TEST(TestRotateX, AsExpected)
{
	const auto axis = Vec3d{1,0,0};
	const auto angle = pi_d/3;
    const auto r1 = Matrix34d::rotate_x(angle);
    const auto r2 = Matrix34d::rotate(axis, angle);

    const auto p1 = Vec3d{0,1,0};

    EXPECT_TRUE(r1.almost(r2, 0.00001));
    EXPECT_TRUE((r2 * p1).almost({0,.5,.866025}, 0.00001));
}

TEST(TestRotateY, AsExpected)
{
	const auto axis = Vec3d{0,1,0};
	const auto angle = pi_d/3;
    const auto r1 = Matrix34d::rotate_y(angle);
    const auto r2 = Matrix34d::rotate(axis, angle);

    const auto p1 = Vec3d{1,0,0};

    EXPECT_TRUE(r1.almost(r2, 0.00001));
    EXPECT_TRUE((r2 * p1).almost({.5,0,-.866025}, 0.00001));
}

TEST(TestRotateZ, AsExpected)
{
	const auto axis = Vec3d{0,0,1};
	const auto angle = pi_d/3;
    const auto r1 = Matrix34d::rotate_z(angle);
    const auto r2 = Matrix34d::rotate(axis, angle);

    const auto p1 = Vec3d{1,0,0};

    EXPECT_TRUE(r1.almost(r2, 0.00001));
    EXPECT_TRUE((r2 * p1).almost({.5,.866025,0}, 0.00001));
}
