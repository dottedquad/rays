#include "../src/vec.h"
#include "gtest/gtest.h"

TEST(TestCross1, AsExpected)
{
    auto v1 = Vec3d{1, 2, 3};
    auto up = Vec3d{0, 1, 0};
    auto right = Vec3d{1, 0, 0};
    auto out = Vec3d{0, 0, 1};

    EXPECT_EQ(right.cross(up), out);
    EXPECT_EQ(up.dot(up), 1.0);
    EXPECT_EQ(up.dot(right), 0.0);
    // EXPECT_EQ(1,2);
}

TEST(TestCross2, AsExpected)
{
    auto v1 = Vec4d{1, 2, 3};
    auto up = Vec4d{0, 1, 0};
    auto right = Vec4d{1, 0, 0};
    auto out = Vec4d{0, 0, 1};

    EXPECT_EQ(right.cross(up), out);
    EXPECT_EQ(up.dot(up), 1.0);
    EXPECT_EQ(up.dot(right), 0.0);
    // EXPECT_EQ(1,2);
}
