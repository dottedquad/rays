#ifndef SCENE_H_
#define SCENE_H_

#include "box.h"
#include "material.h"
#include "ray.h"
#include "sphere.h"
#include "triangle.h"
#include "tristrip.h"
#include <nlohmann/json.hpp>
#include <random>
#include <vector>

struct Light {
    Vec3d pos;
    RGB color;

    Light(const Vec3d& pos, const RGB& color) : pos{pos}, color{color} {}
};

struct Intersection {
public:
    double t;
    Vec3d normal;
    const Material* material;

public:
    Intersection(const Intersection& i) : t{i.t}, normal{i.normal}, material{i.material} {}
    Intersection(const double& tt, const Vec3d& nn, const Material* mm) : t{tt}, normal{nn}, material{mm} {}
    // const Intersection& operator=(const Intersection& other)  {
    //     this->t = other.t;
    //     this->normal = other.normal;
    //     this->material = other.material;

    //     return *this;
    // }
    bool operator<(const Intersection& other) const { return t < other.t; }
    bool operator>=(const Intersection& other) const { return t >= other.t; }
};

class Scene {
public:
    Scene(const std::string& json_string);

    std::vector<Intersection> intersect(const RayD& r) const;

    RGB shade(const std::vector<Intersection>& is, const RayD& r, 
    std::mt19937& gen, std::uniform_real_distribution<>& dis, const int depth = 0) const;

private:

    void parse_objects(const Matrix34d& obj_to_world, const Matrix34d& world_to_obj, const nlohmann::json::const_iterator& json);
    void parse_object(const Matrix34d& obj_to_world, const Matrix34d& world_to_obj, const nlohmann::json& json);

private:

    std::map<std::string, Material> _materials;

    std::vector<std::pair<TriStrip, const Material&>> _tristrips;
    std::vector<std::pair<SphereD, const Material&>> _spheres;
    std::vector<std::pair<Box, const Material&>> _boxes;

    std::vector<Light> _lights;
};

#endif
