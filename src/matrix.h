#ifndef MATRIX_H_
#define MATRIX_H_

#include "vec.h"
#include <cmath>

template <typename T>
class Matrix34;

template <typename T>
class Matrix33 {
public:
    Matrix33(const T& m00,
             const T& m01,
             const T& m02,
             const T& m10,
             const T& m11,
             const T& m12,
             const T& m20,
             const T& m21,
             const T& m22)
        : _m{{m00, m01, m02}, {m10, m11, m12}, {m20, m21, m22}}
    {
    }

    Matrix33(const T (&m)[3][3]) : _m{m} {}

    Matrix33<T> operator*(const Matrix33<T>& o) const
    {
        return {
                _m[0][0] * o._m[0][0] + _m[0][1] * o._m[1][0] + _m[0][2] * o._m[2][0],
                _m[0][0] * o._m[0][1] + _m[0][1] * o._m[1][1] + _m[0][2] * o._m[2][1],
                _m[0][0] * o._m[0][2] + _m[0][1] * o._m[1][2] + _m[0][2] * o._m[2][2],

                _m[1][0] * o._m[0][0] + _m[1][1] * o._m[1][0] + _m[1][2] * o._m[2][0],
                _m[1][0] * o._m[0][1] + _m[1][1] * o._m[1][1] + _m[1][2] * o._m[2][1],
                _m[1][0] * o._m[0][2] + _m[1][1] * o._m[1][2] + _m[1][2] * o._m[2][2],

                _m[2][0] * o._m[0][0] + _m[2][1] * o._m[1][0] + _m[2][2] * o._m[2][0],
                _m[2][0] * o._m[0][1] + _m[2][1] * o._m[1][1] + _m[2][2] * o._m[2][1],
                _m[2][0] * o._m[0][2] + _m[2][1] * o._m[1][2] + _m[2][2] * o._m[2][2],
        };
    }

    Vec3<T> operator*(const Vec3<T>& o) const { return {o.dot(_m[0]), o.dot(_m[1]), o.dot(_m[2])}; }

    bool operator==(const Vec3<T>& other)
    {
        return _m[0][0] == other._m[0][0] && _m[0][1] == other._m[0][1] && _m[0][2] == other._m[0][2] &&
               _m[1][0] == other._m[1][0] && _m[1][1] == other._m[1][1] && _m[1][2] == other._m[1][2] &&
               _m[2][0] == other._m[2][0] && _m[2][1] == other._m[2][1] && _m[2][2] == other._m[2][2];
    }

    bool almost(const Matrix33<T>& other, const T& eps)
    {
        return (std::abs(_m[0][0] - other._m[0][0]) < eps) && (std::abs(_m[0][1] - other._m[0][1]) < eps) &&
               (std::abs(_m[0][2] - other._m[0][2]) < eps) &&

               (std::abs(_m[1][0] - other._m[1][0]) < eps) && (std::abs(_m[1][1] - other._m[1][1]) < eps) &&
               (std::abs(_m[1][2] - other._m[1][2]) < eps) &&

               (std::abs(_m[2][0] - other._m[2][0]) < eps) && (std::abs(_m[2][1] - other._m[2][1]) < eps) &&
               (std::abs(_m[2][2] - other._m[2][2]) < eps);
    }

    static Matrix33<T> rotate_x(const T& angle)
    {
        return {
                1, 0, 0, 0, std::cos(angle), -std::sin(angle), 0, std::sin(angle), std::cos(angle),
        };
    }

    static Matrix33<T> rotate_y(const T& angle)
    {
        return {
                std::cos(angle), 0, std::sin(angle), 0, 1, 0, -std::sin(angle), 0, std::cos(angle),
        };
    }

    static Matrix33<T> rotate_z(const T& angle)
    {
        return {
                std::cos(angle), -std::sin(angle), 0, std::sin(angle), std::cos(angle), 0, 0, 0, 1,
        };
    }

    static Matrix33<T> rotate(const Vec3<T>& axis, const T& angle)
    {
        const T& x = axis.x();
        const T& y = axis.y();
        const T& z = axis.z();
        return {1 + (1 - std::cos(angle)) * (x * x - 1),
                -z * std::sin(angle) + (1 - std::cos(angle)) * x * y,
                y * std::sin(angle) + (1 - std::cos(angle)) * x * z,

                z * std::sin(angle) + (1 - std::cos(angle)) * x * y,
                1 + (1 - std::cos(angle)) * (y * y - 1),
                -x * std::sin(angle) + (1 - std::cos(angle)) * y * z,

                -y * std::sin(angle) + (1 - std::cos(angle)) * x * z,
                x * std::sin(angle) + (1 - std::cos(angle)) * y * z,
                1 + (1 - std::cos(angle)) * (z * z - 1)};
    }

    static Matrix33<T> scale(const Vec3<T>& scale)
    {
        return {
                scale.x(), 0, 0, 0, scale.y(), 0, 0, 0, scale.z(),
        };
    }

    Matrix33<T> transpose() const
    {
        return {
                _m[0][0], _m[1][0], _m[2][0], _m[0][1], _m[1][1], _m[2][1], _m[0][2], _m[1][2], _m[2][2],
        };
    }

    bool almost(const Matrix33<T>& other, const T& eps) const
    {
        return (std::abs(_m[0][0] - other._m[0][0]) < eps) && (std::abs(_m[0][1] - other._m[0][1]) < eps) && (std::abs(_m[0][2] - other._m[0][2]) < eps) && 
        (std::abs(_m[1][0] - other._m[1][0]) < eps) && (std::abs(_m[1][1] - other._m[1][1]) < eps) && (std::abs(_m[1][2] - other._m[1][2]) < eps) && 
        (std::abs(_m[2][0] - other._m[2][0]) < eps) && (std::abs(_m[2][1] - other._m[2][1]) < eps) && (std::abs(_m[2][2] - other._m[2][2]) < eps);
    }

    static Matrix33<T> from_vec(const Vec3<T>& z) {
        const auto x = (std::abs(z.dot(right())) > .99 ? up() : right()).cross(Matrix33<T>::out()).normalize();
        const auto y = z.cross(x).normalize();
        return {x.x(), x.y(), x.z(),
                y.x(), y.y(), y.z(),
                z.x(), z.y(), z.z()};
    }

    Vec3<T> hemisphere_sample(T u, T v) const {
        auto theta = 2 * 3.14159265358979323846264338327950288419716939937510 * u;
        auto radiusSquared = v;
        auto radius = sqrt(radiusSquared);
        return *this * Vec3<T>{std::cos(theta) * radius, std::sin(theta) * radius,
                      std::sqrt(1 - radiusSquared)}.normalize();
    }

    static constexpr Vec3<T> right() { return {1,0,0}; }
    static constexpr Vec3<T> up() { return {0,1,0}; }
    static constexpr Vec3<T> out() { return {0,0,1}; }
    static constexpr Vec3<T> left() { return {-1,0,0}; }
    static constexpr Vec3<T> down() { return {0,-1,0}; }
    static constexpr Vec3<T> in() { return {0,0,-1}; }

public:
    T _m[3][3];
};

using Matrix33ld = Matrix33<long double>;
using Matrix33d = Matrix33<double>;
using Matrix33f = Matrix33<float>;

template <typename T>
class Matrix34 {
public:
    Matrix34(const T& m00,
             const T& m01,
             const T& m02,
             const T& m03,
             const T& m10,
             const T& m11,
             const T& m12,
             const T& m13,
             const T& m20,
             const T& m21,
             const T& m22,
             const T& m23)
        : _m{{m00, m01, m02, m03}, {m10, m11, m12, m13}, {m20, m21, m22, m23}}
    {
    }

    Matrix34(const Matrix33<T>& m)
        : _m{{m._m[0][0], m._m[0][1], m._m[0][2], 0},
             {m._m[1][0], m._m[1][1], m._m[1][2], 0},
             {m._m[2][0], m._m[2][1], m._m[2][2], 0},
             }
    {
    }

    Matrix34(const T (&m)[4][4]) : _m{m} {}

    Matrix34<T> operator*(const Matrix34<T>& o) const
    {
        return {
                _m[0][0] * o._m[0][0] + _m[0][1] * o._m[1][0] + _m[0][2] * o._m[2][0],
                _m[0][0] * o._m[0][1] + _m[0][1] * o._m[1][1] + _m[0][2] * o._m[2][1],
                _m[0][0] * o._m[0][2] + _m[0][1] * o._m[1][2] + _m[0][2] * o._m[2][2],
                _m[0][0] * o._m[0][3] + _m[0][1] * o._m[1][3] + _m[0][2] * o._m[2][3] + _m[0][3],

                _m[1][0] * o._m[0][0] + _m[1][1] * o._m[1][0] + _m[1][2] * o._m[2][0],
                _m[1][0] * o._m[0][1] + _m[1][1] * o._m[1][1] + _m[1][2] * o._m[2][1],
                _m[1][0] * o._m[0][2] + _m[1][1] * o._m[1][2] + _m[1][2] * o._m[2][2],
                _m[1][0] * o._m[0][3] + _m[1][1] * o._m[1][3] + _m[1][2] * o._m[2][3] + _m[1][3],

                _m[2][0] * o._m[0][0] + _m[2][1] * o._m[1][0] + _m[2][2] * o._m[2][0],
                _m[2][0] * o._m[0][1] + _m[2][1] * o._m[1][1] + _m[2][2] * o._m[2][1],
                _m[2][0] * o._m[0][2] + _m[2][1] * o._m[1][2] + _m[2][2] * o._m[2][2],
                _m[2][0] * o._m[0][3] + _m[2][1] * o._m[1][3] + _m[2][2] * o._m[2][3] + _m[2][3],

        };
    }

    Vec3<T> operator*(const Vec3<T>& o) const {
        const auto v = Vec4<T>{o};
        return {v.dot(_m[0]), v.dot(_m[1]), v.dot(_m[2])};
    }
    //Vec4<T> operator*(const Vec4<T>& o) const { return {o.dot(_m[0]), o.dot(_m[1]), o.dot(_m[2]), o.dot(_m[3])}; }

    bool operator==(const Matrix34<T>& other)
    {
        return _m[0][0] == other._m[0][0] && _m[0][1] == other._m[0][1] && _m[0][2] == other._m[0][2] && _m[0][3] == other._m[0][3] &&
               _m[1][0] == other._m[1][0] && _m[1][1] == other._m[1][1] && _m[1][2] == other._m[1][2] && _m[1][3] == other._m[1][3] &&
               _m[2][0] == other._m[2][0] && _m[2][1] == other._m[2][1] && _m[2][2] == other._m[2][2] && _m[2][3] == other._m[2][3];
    }

    bool almost(const Matrix33<T>& other, const T& eps)
    {
        return (std::abs(_m[0][0] - other._m[0][0]) < eps) && (std::abs(_m[0][1] - other._m[0][1]) < eps) &&
               (std::abs(_m[0][2] - other._m[0][2]) < eps) &&

               (std::abs(_m[1][0] - other._m[1][0]) < eps) && (std::abs(_m[1][1] - other._m[1][1]) < eps) &&
               (std::abs(_m[1][2] - other._m[1][2]) < eps) &&

               (std::abs(_m[2][0] - other._m[2][0]) < eps) && (std::abs(_m[2][1] - other._m[2][1]) < eps) &&
               (std::abs(_m[2][2] - other._m[2][2]) < eps);
    }

    bool almost(const Matrix34<T>& other, const T& eps) const
    {
        return (std::abs(_m[0][0] - other._m[0][0]) < eps) && (std::abs(_m[0][1] - other._m[0][1]) < eps) && (std::abs(_m[0][2] - other._m[0][2]) < eps) &&  (std::abs(_m[0][3] - other._m[0][3]) < eps) && 
(std::abs(_m[1][0] - other._m[1][0]) < eps) && (std::abs(_m[1][1] - other._m[1][1]) < eps) && (std::abs(_m[1][2] - other._m[1][2]) < eps) &&  (std::abs(_m[1][3] - other._m[1][3]) < eps) && 
(std::abs(_m[2][0] - other._m[2][0]) < eps) && (std::abs(_m[2][1] - other._m[2][1]) < eps) && (std::abs(_m[2][2] - other._m[2][2]) < eps) &&  (std::abs(_m[2][3] - other._m[2][3]) < eps);
    }


    static Matrix34<T> rotate_x(const T& angle) { return Matrix33<T>::rotate_x(angle); }

    static Matrix34<T> rotate_y(const T& angle) { return Matrix33<T>::rotate_y(angle); }

    static Matrix34<T> rotate_z(const T& angle) { return Matrix33<T>::rotate_z(angle); }

    static Matrix34<T> rotate(const Vec3<T>& axis, const T& angle) { return Matrix33<T>::rotate(axis, angle); }

    static Matrix34<T> translate(const Vec3<T>& translation)
    {
        return {
                1, 0, 0, translation.x(), 0, 1, 0, translation.y(), 0, 0, 1, translation.z(),
        };
    }

    static Matrix34<T> scale(const Vec3<T>& scale) { return Matrix33<T>::scale(scale); }

    T trace() const { return _m[0][0] + _m[1][1] + _m[2][2] + _m[3][3]; }

    static Matrix34<T> identity()
    {
        return {
                1, 0, 0, 0, 0, 1, 0, 0, 0, 0, 1, 0,
        };
    }

    // Matrix34<T> transpose() const
    // {
    //     return {
    //             _m[0][0], _m[1][0], _m[2][0], _m[3][0], _m[0][1], _m[1][1], _m[2][1], _m[3][1],
    //             _m[0][2], _m[1][2], _m[2][2], _m[3][2], _m[0][3], _m[1][3], _m[2][3], _m[3][3],
    //     };
    // }

    Vec3<T> normal_transform(const Vec3<T>& v) const
    {
        return {v.dot(Vec3d{_m[0][0],_m[0][1],_m[0][2]}),
                v.dot(Vec3d{_m[1][0],_m[1][1],_m[1][2]}),
                v.dot(Vec3d{_m[2][0],_m[2][1],_m[2][2]})};
    }

    Vec3<T> normal_transposed_transform(const Vec3<T>& v) const
    {
        return {v.dot(Vec3d{_m[0][0],_m[1][0],_m[2][0]}),
                v.dot(Vec3d{_m[0][1],_m[1][1],_m[2][1]}),
                v.dot(Vec3d{_m[0][2],_m[1][2],_m[2][2]})};
    }

    // Matrix33<T> cofactor(size_t c_row, size_t c_col) const
    // {
    //     int i=0;
    //     int j=0;
    //     Matrix33 c;

    //     for (int row=0; row<4; ++row) {
    //         if (row != c_row) {
    //             for (int col=0; col<4; ++col) {
    //                 if (col != c_col) {
    //                     c._m[i][j] = _m[row][col];
    //                     j++;

    //                     if (j == 3) {
    //                         j = 0;
    //                         i++;
    //                     }
    //                 }
    //             }
    //         }
    //     }
    // }

    // T determinate() const
    // {
    //     T d;
    //     Matrix34<T> temp;
    //     T sign = 1;

    //     for (int f=0; f<4; ++f) {
    //         this->cofactor(temp, 0, f);
    //         determinate += sign * _m[0][f] *
    //     }

    //     return _m[0][0] + _m[1][1] + _m[2][2] + _m[3][3];
    // }

public:
    T _m[3][4];
};

using Matrix34ld = Matrix34<long double>;
using Matrix34d = Matrix34<double>;
using Matrix34f = Matrix34<float>;

constexpr long double pi_ld = 3.14159265358979323846264338327950288419716939937510L;
constexpr double pi_d = 3.14159265358979323846264338327950288419716939937510;
constexpr float pi_f = 3.14159265358979323846264338327950288419716939937510f;

#endif
