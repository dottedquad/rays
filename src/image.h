#ifndef IMAGE_H_
#define IMAGE_H_

#include <ostream>
#include <vector>

struct Pixel {
    float r;
    float g;
    float b;

    Pixel operator+(const Pixel& o) const {
        return {r + o.r, g + o.g, b + o.g};  
    }
    Pixel& operator+=(const Pixel& o) {
        r += o.r;
        g += o.g;
        b += o.b;
        return *this;
    }
    Pixel& operator/=(size_t num) {
        r /= num;
        g /= num;
        b /= num;
        return *this;
    }
};

class Image {
public:
    Image(const size_t width, const size_t height) : _buffer(width * height), _width(width), _height(height) {}

    Pixel& pixel(const size_t x, const size_t y) { return _buffer[y * _width + x]; }
    const Pixel& pixel(const size_t x, const size_t y) const { return _buffer[y * _width + x]; }

    friend std::ostream& operator<<(std::ostream& os, const Image& i);

private:
    std::vector<Pixel> _buffer;
    size_t _width;
    size_t _height;
};

std::ostream& operator<<(std::ostream& os, const Image& i)
{
    os << "PF\n" << i._width << " " << i._height << "\n-1.0\n";
    os.write((char*) &i._buffer[0], i._buffer.size() * sizeof(Pixel));
    return os;
}

#endif
