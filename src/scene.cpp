#include "scene.h"
#include "object.h"
#include <algorithm>
#include <exception>
#include <iostream>
#include <nlohmann/json.hpp>
#include <vector>

#include "debug_cout.h"
#include <iostream>

using json = nlohmann::json;

const Material m{{.2, 0, 0}, {.8, 0, 0}, {1, 1, 1}};

static std::map<std::string, Material> parse_materials(const nlohmann::json& json);
static std::vector<Light> parse_lights(const nlohmann::json& json);

// inline std::ostream& operator<<(std::ostream& os, const Intersection& i) {
//  os << "t: " << i.t << " n: " << i.normal;
// }

// inline std::ostream& operator<<(std::ostream& os, const std::vector<Intersection>& is) {
//  os << "intersections " << is.size() << "\n";
//  for (const auto& i: is) {
//      os << i << "\n";
//  }   
// }


Scene::Scene(const std::string& json_string)
{
    const auto scene = json::parse(json_string);
    //std::cout << "scene - " << scene.dump(4) << "\n";

    //std::cout << "Scene\n";

    const auto& materials_itr = scene.find("materials");
    if (materials_itr != scene.end()) { _materials = parse_materials(*materials_itr); }
    else {
        throw std::runtime_error{"No materials"};
    }

    const auto& objects_itr = scene.find("objects");
    if (objects_itr != scene.end()) {
        const auto i = Matrix34d::identity();
        parse_objects(i, i, objects_itr);
    }

    const auto& lights_itr = scene.find("lights");
    if (lights_itr != scene.end()) { _lights = parse_lights(*lights_itr); }
    else {
        throw std::runtime_error{"No lights"};
    }

    //const auto& camera = scene["camera"];


    for (const auto& light : _lights) { std::cout << "light " << light.pos << " " << light.color << "\n"; }

    int i = 0;
    for (const auto& sphere : _spheres) {
        std::cout << i++ << "\n";
        std::cout << sphere.first.obj_to_world() << "\n";
        std::cout << sphere.first.world_to_obj() << "\n";
    }
}

std::vector<Intersection> Scene::intersect(const RayD& r) const
{
    std::vector<Intersection> intersections;
    for (size_t idx = 0; idx < _tristrips.size(); ++idx) {
        const auto& tristrip = _tristrips[idx];
        auto is = tristrip.first.intersect(r);
        for (const auto& i: is) {
            if (i.first >= 0) { intersections.push_back({i.first, i.second, &tristrip.second}); }
        }
    }
    for (size_t idx = 0; idx < _spheres.size(); ++idx) {
        const auto& sphere = _spheres[idx];
        auto is = sphere.first.intersect(r);
        for (const auto& i : is) {
            if (i.first >= 0) { intersections.push_back({i.first, i.second, &sphere.second}); }
        }
    }
    for (size_t idx = 0; idx < _boxes.size(); ++idx) {
        const auto& box = _boxes[idx];
        if (auto i = box.first.intersect(r)) {
            if (i->first >= 0) { intersections.push_back({i->first, i->second, &box.second}); }
        }
    }

    std::sort(intersections.begin(), intersections.end());

    return intersections;
}

RGB Scene::shade(const std::vector<Intersection>& is, const RayD& r,  
    std::mt19937& gen, std::uniform_real_distribution<>& dis, const int depth) const
{
    if (depth > 10) { return {0, 0, 0}; }

    for (const auto& d : is) {
        if (d.t > 0) {
            const auto& t = d.t;
            const auto& i = d;

            if (t <= 0) {
                return {0, 0, 0};
            }
            else {
                //const auto intersection = r.destination(i.t);
                const auto normal = i.normal;
                Vec3f color{0, 0, 0};

                const Vec3f ambient{i.material->ambient.r, i.material->ambient.g, i.material->ambient.b};
                const Vec3f diffuse{i.material->diffuse.r, i.material->diffuse.g, i.material->diffuse.b};
                const Vec3f specular{i.material->specular.r, i.material->specular.g, i.material->specular.b};

                color += ambient;

                bool has_specular = specular.x() > 0.00001 || specular.y() > 0.00001 ||
                                    specular.z() > 0.00001;

                if (has_specular) {
                    const auto almost_intersection = r.destination(i.t - 0.000001);
                    const auto reflected_ray =
                            RayD{almost_intersection, r.direction() - 2 * r.direction().dot(normal) * normal};

                    auto reflection_is = this->intersect(reflected_ray);
                    std::sort(reflection_is.begin(), reflection_is.end());
                    const auto reflected_color = this->shade(reflection_is, reflected_ray, gen, dis, depth + 1);
                    const Vec3f r_c_vec{reflected_color.r, reflected_color.g, reflected_color.b};
                    color._v[0] += specular._v[0] * r_c_vec._v[0];
                    color._v[1] += specular._v[1] * r_c_vec._v[1];
                    color._v[2] += specular._v[2] * r_c_vec._v[2];
                }

                bool has_diffuse = diffuse.x() > 0.00001 || diffuse.y() > 0.00001 ||
                                   diffuse.z() > 0.00001;

                if (has_diffuse) {
                    const auto almost_intersection = r.destination(i.t - 0.000001);
                    auto m = Matrix33d::from_vec(normal);
                    const auto reflected_ray = RayD{almost_intersection, m.hemisphere_sample(dis(gen), dis(gen))};

                    auto reflection_is = this->intersect(reflected_ray);
                    std::sort(reflection_is.begin(), reflection_is.end());
                    const auto reflected_color = this->shade(reflection_is, reflected_ray, gen, dis, depth + 1);
                    const Vec3f r_c_vec{reflected_color.r, reflected_color.g, reflected_color.b};
                    color._v[0] += diffuse._v[0] * r_c_vec._v[0];
                    color._v[1] += diffuse._v[1] * r_c_vec._v[1];
                    color._v[2] += diffuse._v[2] * r_c_vec._v[2];
                }

                /*
                for (const auto& light : _lights) {
                    const Vec3d light_color{light.color.r, light.color.g, light.color.b};
                    const auto dir_to_light = (light.pos - intersection).normalize();
                    float fact = normal.dot(dir_to_light);

                    if (fact > 0 ) {
                    const auto L = dir_to_light;

                    const auto shadow_ray = RayD{intersection, L};
                    std::vector<Intersection> shadow = this->intersect(shadow_ray);

                    bool in_shadow = false;

                    for (const auto& shadow_check : shadow) {
                        if (fact <= 0 || shadow_check.t >= 0.00001) {
                            in_shadow = true;
                            break;
                        }
                    }
                    if (!in_shadow) {
                        color += fact * diffuse; // + hf * specular;
                    }
                }
                }*/

                return color;
            }

            break;
        }
    }

    return {0,0,0};
}

static Material parse_material(const nlohmann::json& json)
{
    RGB ambient{0, 0, 0};
    RGB diffuse{0, 0, 0};
    RGB specular{0, 0, 0};

    const auto& ambient_itr = json.find("ambient");
    if (ambient_itr != json.end()) {
        if (ambient_itr->is_array() && ambient_itr->size() >= 3) {
            ambient = {ambient_itr->at(0).get<float>(), ambient_itr->at(1).get<float>(),
                       ambient_itr->at(2).get<float>()};
        }
        else if (ambient_itr->is_number()) {
            const auto num = ambient_itr->get<float>();
            ambient = {num, num, num};
        }
    }
    const auto& diffuse_itr = json.find("diffuse");
    if (diffuse_itr != json.end()) {
        if (diffuse_itr->is_array() && diffuse_itr->size() >= 3) {
            diffuse = {diffuse_itr->at(0).get<float>(), diffuse_itr->at(1).get<float>(),
                       diffuse_itr->at(2).get<float>()};
        }
        else if (diffuse_itr->is_number()) {
            const auto num = diffuse_itr->get<float>();
            diffuse = {num, num, num};
        }
    }
    const auto& specular_itr = json.find("specular");
    if (specular_itr != json.end()) {
        if (specular_itr->is_array() && specular_itr->size() >= 3) {
            specular = {specular_itr->at(0).get<float>(), specular_itr->at(1).get<float>(),
                        specular_itr->at(2).get<float>()};
        }
        else if (specular_itr->is_number()) {
            const auto num = specular_itr->get<float>();
            specular = {num, num, num};
        }
    }

    return Material{ambient, diffuse, specular};
}

static std::map<std::string, Material> parse_materials(const nlohmann::json& json)
{
    auto materials = std::map<std::string, Material>{};

    for (auto itr = json.cbegin(); itr != json.cend(); ++itr) {
        std::cout << itr.key() << '\n';
        materials.insert({itr.key(), parse_material(itr.value())});
    }

    return materials;
}

static Matrix34d parse_obj_to_world(const nlohmann::json& json)
{
    auto translation_itr = json.find("translate");
    auto translation = Matrix34d::identity();
    if (translation_itr != json.end() && translation_itr->is_array() && translation_itr->size() >= 3) {
        translation = Matrix34d::translate({translation_itr->at(0).get<double>(), translation_itr->at(1).get<double>(),
                                            translation_itr->at(2).get<double>()});
    }

    auto scale_itr = json.find("scale");
    auto scale = Matrix34d::identity();
    if (scale_itr != json.end()) {
        if (scale_itr->is_array() && scale_itr->size() >= 3) {
            scale = Matrix34d::scale(
                    {scale_itr->at(0).get<double>(), scale_itr->at(1).get<double>(), scale_itr->at(2).get<double>()});
        }
        else if (scale_itr->is_number()) {
            const auto scale_num = scale_itr->get<double>();
            scale = Matrix34d::scale({scale_num, scale_num, scale_num});
        }
    }

    auto rotation_itr = json.find("rotate");
    auto rotation = Matrix34d::identity();
    if (rotation_itr != json.end() && rotation_itr->is_object()) {
        auto axis = Vec3d{0, 0, 0};
        double angle = 0;
        auto x_itr = rotation_itr->find("x");
        if (x_itr != rotation_itr->end()) { axis.x() = x_itr->get<double>(); }
        auto y_itr = rotation_itr->find("y");
        if (y_itr != rotation_itr->end()) { axis.y() = y_itr->get<double>(); }
        auto z_itr = rotation_itr->find("z");
        if (z_itr != rotation_itr->end()) { axis.z() = z_itr->get<double>(); }
        auto degrees_itr = rotation_itr->find("degrees");
        if (degrees_itr != rotation_itr->end()) { angle = pi_d * degrees_itr->get<double>() / 180.0; }

        if (axis.length() > 0) { rotation = Matrix34d::rotate(axis.normalize(), angle); }
        std::cout << "rot " << rotation << "\n";
    }

    return translation * rotation * scale;
}

static Matrix34d parse_world_to_obj(const nlohmann::json& json)
{
    auto translation_itr = json.find("translate");
    auto translation = Matrix34d::identity();
    if (translation_itr != json.end()) {
        if (translation_itr->is_array() && translation_itr->size() >= 3) {
            translation =
                    Matrix34d::translate({-translation_itr->at(0).get<double>(), -translation_itr->at(1).get<double>(),
                                          -translation_itr->at(2).get<double>()});
        }
    }

    auto scale_itr = json.find("scale");
    auto scale = Matrix34d::identity();
    if (scale_itr != json.end()) {
        if (scale_itr->is_array() && scale_itr->size() >= 3) {
            scale = Matrix34d::scale({1 / scale_itr->at(0).get<double>(), 1 / scale_itr->at(1).get<double>(),
                                      1 / scale_itr->at(2).get<double>()});
        }
        else if (scale_itr->is_number()) {
            const auto scale_num = 1 / scale_itr->get<double>();
            scale = Matrix34d::scale({scale_num, scale_num, scale_num});
        }
    }

    auto rotation_itr = json.find("rotate");
    auto rotation = Matrix34d::identity();
    if (rotation_itr != json.end() && rotation_itr->is_object()) {
        auto axis = Vec3d{0, 0, 0};
        double angle = 0;
        auto x_itr = rotation_itr->find("x");
        if (x_itr != rotation_itr->end()) { axis.x() = x_itr->get<double>(); }
        auto y_itr = rotation_itr->find("y");
        if (y_itr != rotation_itr->end()) { axis.y() = y_itr->get<double>(); }
        auto z_itr = rotation_itr->find("z");
        if (z_itr != rotation_itr->end()) { axis.z() = z_itr->get<double>(); }
        auto degrees_itr = rotation_itr->find("degrees");
        if (degrees_itr != rotation_itr->end()) { angle = -pi_d * degrees_itr->get<double>() / 180.0; }

        if (axis.length() > 0) { rotation = Matrix34d::rotate(axis.normalize(), angle); }
        std::cout << "irot " << rotation << "\n";
    }


    return scale * rotation * translation;
}

void Scene::parse_objects(const Matrix34d& obj_to_world,
                          const Matrix34d& world_to_obj,
                          const nlohmann::json::const_iterator& objects_itr)
{
    for (auto itr = objects_itr->begin(); itr != objects_itr->end(); ++itr) {
        std::cout << itr.key() << '\n';
        std::cout << itr.value() << '\n';
        parse_object(obj_to_world, world_to_obj, itr.value());
        std::cout << "out - " << itr.key() << '\n';
    }
}

// static Triangle parse_triangle(const Matrix34d& obj_to_world, const Matrix34d& world_to_obj, const nlohmann::json& json)
// {
//     std::cout << __func__ << '\n';
//     auto vertices_itr = json.find("vertices");
//     if (vertices_itr != json.end() && vertices_itr->is_array() && vertices_itr->size() >= 3) {
//         std::cout << "A" << '\n';
//         Vec3d ps[3] = {{0,0,0},{0,0,0},{0,0,0}};
//         for (size_t i=0; i<3; ++i) {
//             const auto& p = vertices_itr->at(i);
//             if (p.size() >=3) {
//                 ps[i] = {p.at(0),p.at(1),p.at(2)};
//             }
//         }
//         return Triangle{ps[0],ps[1],ps[2],obj_to_world,world_to_obj};
//     }
//     throw std::runtime_error("bad triangle");
// }

static TriStrip parse_tristrip(const Matrix34d& obj_to_world, const Matrix34d& world_to_obj, const nlohmann::json& json)
{
    std::cout << __func__ << '\n';
    auto vertices_itr = json.find("vertices");
    if (vertices_itr != json.end() && vertices_itr->is_array() && vertices_itr->size() >= 3) {
        std::cout << "A" << '\n';
        std::vector<Vec3d> ps;
        for (size_t i=0; i<3; ++i) {
            const auto& p = vertices_itr->at(i);
            if (p.size() >=3) {
                ps.push_back({p.at(0),p.at(1),p.at(2)});
            }
        }
        return TriStrip{ps,obj_to_world,world_to_obj};
    }
    throw std::runtime_error("bad triangle");
}

void Scene::parse_object(const Matrix34d& obj_to_world, const Matrix34d& world_to_obj, const nlohmann::json& json)
{
    std::cout << __func__ << '\n';
    // std::cout << json.dump(4) << "\n";

    const auto cur_obj_to_world = obj_to_world * parse_obj_to_world(json);
    const auto cur_world_to_obj = parse_world_to_obj(json) * world_to_obj;

    auto type_itr = json.find("type");
    if (type_itr != json.end() && type_itr->is_string()) {
        auto material_itr = json.find("material");
        if (material_itr != json.end() && material_itr->is_string() && _materials.count(*material_itr) > 0) {
            const auto type = type_itr->get<std::string>();
            if (type == "sphere") {
                //{cur_obj_to_world, cur_world_to_obj}, _materials.find(*material_itr)->second
                _spheres.push_back({{cur_obj_to_world, cur_world_to_obj}, _materials.find(*material_itr)->second});
            }
            else if (type == "tristrip") {
                _tristrips.push_back({parse_tristrip(cur_obj_to_world, cur_world_to_obj, json), _materials.find(*material_itr)->second});
            }
            else if (type == "box") {
                _boxes.push_back({{cur_obj_to_world, cur_world_to_obj}, _materials.find(*material_itr)->second});
            }
        }
        else {
            throw std::runtime_error{"can't find material"};
        }
    }

    auto children_itr = json.find("children");
    if (children_itr != json.end()) { parse_objects(cur_obj_to_world, cur_world_to_obj, children_itr); }
}

static Light parse_light(const nlohmann::json& json)
{
    RGB color{0, 0, 0};
    Vec3d position{0, 0, 0};

    const auto& intensity_itr = json.find("intensity");
    if (intensity_itr != json.end()) {
        if (intensity_itr->is_array() && intensity_itr->size() >= 3) {
            color = {intensity_itr->at(0).get<float>(), intensity_itr->at(1).get<float>(),
                     intensity_itr->at(2).get<float>()};
        }
        else if (intensity_itr->is_number()) {
            const auto num = intensity_itr->get<float>();
            color = {num, num, num};
        }
    }
    const auto& position_itr = json.find("position");
    if (position_itr != json.end()) {
        if (position_itr->is_array() && position_itr->size() >= 3) {
            position = {position_itr->at(0).get<float>(), position_itr->at(1).get<float>(),
                        position_itr->at(2).get<float>()};
        }
        else if (position_itr->is_number()) {
            const auto num = position_itr->get<float>();
            position = {num, num, num};
        }
    }

    return Light{position, color};
}

static std::vector<Light> parse_lights(const nlohmann::json& json)
{
    auto lights = std::vector<Light>{};

    for (auto itr = json.cbegin(); itr != json.cend(); ++itr) {
        std::cout << itr.key() << '\n';
        lights.push_back(parse_light(itr.value()));
    }

    return lights;
}
