#ifndef CAMERA_H_
#define CAMERA_H_

#include "ray.h"
#include "vec.h"
#include <cmath>
// #include <iostream>
// #include <debug_cout.h>

class Camera {
public:
    Camera(const Vec3d& vp,
           const Vec3d& top_left,
           const Vec3d& top_right,
           const Vec3d& bottom_left,
           const Vec3d& bottom_right)
        : _vp{vp}, _top_left{top_left}, _top_right{top_right}, _bottom_left{bottom_left}, _bottom_right{bottom_right}
    {
    }

    Camera(const Vec3d& vp, const Vec3d& look_at, const double vert_angle, const double horz_angle)
        : _vp{vp}, _top_left{0, 0, 0}, _top_right{0, 0, 0}, _bottom_left{0, 0, 0}, _bottom_right{0, 0, 0}
    {
        const auto look_dir = (look_at - vp).normalize();
        const auto up = Vec3d{0, 1, 0};
        const auto cam_right = look_dir.cross(up).normalize();
        const auto cam_up = cam_right.cross(look_dir);
        const auto vert_step = std::tan(vert_angle / 2.0);
        const auto horz_step = std::tan(horz_angle / 2.0);
        const auto look_point = _vp + look_dir;
        _top_left = look_point + vert_step * cam_up - horz_step * cam_right;
        _top_right = look_point + vert_step * cam_up + horz_step * cam_right;
        _bottom_left = look_point - vert_step * cam_up - horz_step * cam_right;
        _bottom_right = look_point - vert_step * cam_up + horz_step * cam_right;
        // std::cout << "look_dir" << look_dir << "\n";
        // std::cout << "look_at" << look_at << "\n";
        // std::cout << "look_point" << look_point << "\n";
        // std::cout << "vert_step" << vert_step << "\n";
        // std::cout << "horz_step" << horz_step << "\n";
        // std::cout << "bottom_left" << _bottom_left << "\n";
    }

    const Vec3d& viewpoint() const { return _vp; }

    RayD get_ray(double x, double y) const
    {
        return {_vp,
                (((1 - y) * ((1 - x) * _bottom_left + x * _bottom_right) + y * ((1 - x) * _top_left + x * _top_right)) -
                 _vp)
                        .normalize()};
    }

private:
    Vec3d _vp;
    Vec3d _top_left;
    Vec3d _top_right;
    Vec3d _bottom_left;
    Vec3d _bottom_right;
};

#endif