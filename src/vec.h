#ifndef VEC_H_
#define VEC_H_

#include <cmath>

template <typename T>
class Vec3 {
public:
    Vec3(const T& x, const T& y, const T& z) : _v{x, y, z} {}

    Vec3(const T (&v)[3])
    {
        _v[0] = v[0];
        _v[1] = v[1];
        _v[2] = v[2];
    }
    Vec3(const Vec3& v) = default;

    T _v[3];

public:
    Vec3<T> normalize() const
    {
        auto inv_length = 1 / this->length();
        return *this * inv_length;
    }

    T dot(const Vec3<T>& other) const { return _v[0] * other._v[0] + _v[1] * other._v[1] + _v[2] * other._v[2]; }

    Vec3<T> operator*(const T& scale) const { return {scale * _v[0], scale * _v[1], scale * _v[2]}; }

    Vec3<T>& operator+=(const Vec3<T>& other)
    {
        *this = *this + other;
        return *this;
    }

    Vec3<T> cross(const Vec3<T>& other) const
    {
        return {
                _v[1] * other._v[2] - _v[2] * other._v[1],
                _v[2] * other._v[0] - _v[0] * other._v[2],
                _v[0] * other._v[1] - _v[1] * other._v[0],
        };
    }

    Vec3<T> operator+(const Vec3<T>& other) const
    {
        return {
                _v[0] + other._v[0],
                _v[1] + other._v[1],
                _v[2] + other._v[2],
        };
    }

    Vec3<T> operator-(const Vec3<T>& other) const
    {
        return {
                _v[0] - other._v[0],
                _v[1] - other._v[1],
                _v[2] - other._v[2],
        };
    }

    Vec3<T> operator-() const { return T{-1} * *this; }

    const T& x() const { return _v[0]; }
    const T& y() const { return _v[1]; }
    const T& z() const { return _v[2]; }

    T& x() { return _v[0]; }
    T& y() { return _v[1]; }
    T& z() { return _v[2]; }

    T length() const { return std::sqrt(this->dot(*this)); }

    bool operator==(const Vec3<T>& other) const
    {
        return _v[0] == other._v[0] && _v[1] == other._v[1] && _v[2] == other._v[2];
    }

    bool almost(const Vec3<T>& other, const T& eps) const
    {
        return (std::abs(_v[0] - other._v[0]) < eps) && (std::abs(_v[1] - other._v[1]) < eps) &&
               (std::abs(_v[2] - other._v[2]) < eps);
    }
};

template <typename T>
Vec3<T> operator/(const T& scale, const Vec3<T>& vec) {
    return {scale / vec._v[0], scale / vec._v[1], scale / vec._v[2]};
}

template <typename T>
Vec3<T> operator*(const T& scale, const Vec3<T>& vec)
{
    return vec * scale;
}

using Vec3ld = Vec3<long double>;
using Vec3d = Vec3<double>;
using Vec3f = Vec3<float>;


template <typename T>
class Vec4 {
public:
    Vec4(const T& x, const T& y, const T& z, const T& w) : _v{x, y, z, w} {}
    Vec4(const T& x, const T& y, const T& z) : _v{x, y, z, 0} {}

    Vec4(const Vec3<T> v)
    {
        _v[0] = v._v[0];
        _v[1] = v._v[1];
        _v[2] = v._v[2];
        _v[3] = 1;
    }

    Vec4(const T (&v)[4])
    {
        _v[0] = v[0];
        _v[1] = v[1];
        _v[2] = v[2];
        _v[3] = v[3];
    }

    Vec4(const Vec4& v) = default;

    Vec4<T> normalize() const
    {
        auto inv_length = 1 / this->length();
        return *this * inv_length;
    }

    T dot(const Vec4<T>& other) const
    {
        return _v[0] * other._v[0] + _v[1] * other._v[1] + _v[2] * other._v[2] + _v[3] * other._v[3];
    }
    T dot(const Vec3<T>& other) const { return _v[0] * other._v[0] + _v[1] * other._v[1] + _v[2] * other._v[2]; }

    Vec4<T> operator*(const T& scale) const { return {scale * _v[0], scale * _v[1], scale * _v[2], scale * _v[3]}; }

    Vec4<T>& operator+=(const Vec4<T>& other)
    {
        *this = *this + other;
        return *this;
    }

    // cross products only make sense in 3D, so this just assume that w is 0
    Vec4<T> cross(const Vec4<T>& other) const
    {
        return {
                _v[1] * other._v[2] - _v[2] * other._v[1],
                _v[2] * other._v[0] - _v[0] * other._v[2],
                _v[0] * other._v[1] - _v[1] * other._v[0],
                0,
        };
    }

    Vec4<T> cross(const Vec3<T>& other) const
    {
        return {
                _v[1] * other._v[2] - _v[2] * other._v[1],
                _v[2] * other._v[0] - _v[0] * other._v[2],
                _v[0] * other._v[1] - _v[1] * other._v[0],
                0,
        };
    }

    Vec4<T> operator+(const Vec4<T>& other) const
    {
        return {
                _v[0] + other._v[0],
                _v[1] + other._v[1],
                _v[2] + other._v[2],
                _v[3] + other._v[3],
        };
    }

    Vec4<T> operator-(const Vec4<T>& other) const
    {
        return {
                _v[0] - other._v[0],
                _v[1] - other._v[1],
                _v[2] - other._v[2],
                _v[3] - other._v[3],
        };
    }

    Vec4<T> operator-() const { return -1 * this; }

    const T& x() const { return _v[0]; }
    const T& y() const { return _v[1]; }
    const T& z() const { return _v[2]; }
    const T& w() const { return _v[3]; }

    T length() const { return std::sqrt(this->dot(*this)); }

    bool operator==(const Vec4<T>& other) const
    {
        return _v[0] == other._v[0] && _v[1] == other._v[1] && _v[2] == other._v[2] && _v[3] == other._v[3];
    }

    bool almost(const Vec4<T>& other, const T& eps)
    {
        return (std::abs(_v[0] - other._v[0]) < eps) && (std::abs(_v[1] - other._v[1]) < eps) &&
               (std::abs(_v[2] - other._v[2]) < eps) && (std::abs(_v[3] - other._v[3]) < eps);
    }

public:
    T _v[4];
};

template <typename T>
Vec4<T> operator*(const T& scale, const Vec4<T>& vec)
{
    return vec * scale;
}

using Vec4ld = Vec4<long double>;
using Vec4d = Vec4<double>;
using Vec4f = Vec4<float>;


#endif
