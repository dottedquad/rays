#include "object.h"
#include "scene.h"
#include "sphere.h"

#include <nlohmann/json.hpp>
#include <vector>

#include <iostream>

using json = nlohmann::json;

static Matrix34d parse_obj_to_world(const nlohmann::json& json);
static Matrix34d parse_world_to_obj(const nlohmann::json& json);
//static SphereD parse_sphere(const nlohmann::json& json);

Object::Object() : _obj_to_world{Matrix34d::identity()}, _world_to_obj{Matrix34d::identity()} {}

Object::Object(const Matrix34d& parent_obj_to_world, const Matrix34d& parent_world_to_obj, const nlohmann::json& json)
    : _obj_to_world{parent_obj_to_world}, _world_to_obj{parent_world_to_obj}
{
    std::cout << "Object::Object" << '\n';
    std::cout << json.dump(4) << "\n";

    _obj_to_world = _obj_to_world * parse_obj_to_world(json);
    _world_to_obj = parse_world_to_obj(json) * _world_to_obj;

    auto type_itr = json.find("type");
    if (type_itr != json.end() && type_itr->is_string()) {
    	const auto type = type_itr->get<std::string>();
    	if (type == "sphere") {
    		_spheres.push_back({_obj_to_world, _world_to_obj});
    	}
    }

    auto children_itr = json.find("children");
    if (children_itr != json.end()) {
        for (auto itr = children_itr->begin(); itr != children_itr->end(); ++itr) {
            std::cout << itr.key() << '\n';
            std::cout << itr.value() << '\n';
            _children.push_back({_obj_to_world, _world_to_obj, itr.value()});
            std::cout << "out - " << itr.key() << '\n';
        }
    }
}

void Object::add_child(const nlohmann::json& json)
{
    _children.push_back({_obj_to_world, _world_to_obj, json});
}

static Matrix34d parse_obj_to_world(const nlohmann::json& json)
{
    auto translation_itr = json.find("translation");
    auto translation = Matrix34d::identity();
    if (translation_itr != json.end() && translation_itr->is_array() && translation_itr->size() >= 3) {
        translation = Matrix34d::translate({translation_itr->at(0).get<double>(), translation_itr->at(1).get<double>(),
                                            translation_itr->at(2).get<double>()});
    }

    auto scale_itr = json.find("scale");
    auto scale = Matrix34d::identity();
    if (scale_itr != json.end()) {
        if (scale_itr->is_array() && scale_itr->size() >= 3) {
            scale = Matrix34d::scale(
                    {scale_itr->at(0).get<double>(), scale_itr->at(1).get<double>(), scale_itr->at(2).get<double>()});
        }
        else if (scale_itr->is_number()) {
            const auto scale_num = scale_itr->get<double>();
            scale = Matrix34d::scale({scale_num, scale_num, scale_num});
        }
    }

    auto rotation_itr = json.find("rotation");
    auto rotation = Matrix34d::identity();
    if (rotation_itr != json.end() && rotation_itr->is_object()) {
        auto axis = Vec3d{0, 0, 0};
        double angle = 0;
        auto x_itr = rotation_itr->find("x");
        if (x_itr != rotation_itr->end()) { axis.x() = x_itr->get<double>(); }
        auto y_itr = rotation_itr->find("y");
        if (y_itr != rotation_itr->end()) { axis.y() = y_itr->get<double>(); }
        auto z_itr = rotation_itr->find("z");
        if (z_itr != rotation_itr->end()) { axis.z() = z_itr->get<double>(); }
        auto degrees_itr = rotation_itr->find("degrees");
        if (degrees_itr != rotation_itr->end()) { angle = pi_d * degrees_itr->get<double>() / 180.0; }

        if (axis.length() > 0) { rotation = Matrix34d::rotate(axis.normalize(), angle); }
    }

    return translation * rotation * scale;
}

static Matrix34d parse_world_to_obj(const nlohmann::json& json)
{
    auto translation_itr = json.find("translation");
    auto translation = Matrix34d::identity();
    if (translation_itr != json.end()) {
        if (translation_itr->is_array() && translation_itr->size() >= 3) {
            translation =
                    Matrix34d::translate({-translation_itr->at(0).get<double>(), -translation_itr->at(1).get<double>(),
                                          -translation_itr->at(2).get<double>()});
        }
    }

    auto scale_itr = json.find("scale");
    auto scale = Matrix34d::identity();
    if (scale_itr != json.end()) {
        if (scale_itr->is_array() && scale_itr->size() >= 3) {
            scale = Matrix34d::scale({1 / scale_itr->at(0).get<double>(), 1 / scale_itr->at(1).get<double>(),
                                      1 / scale_itr->at(2).get<double>()});
        }
        else if (scale_itr->is_number()) {
            const auto scale_num = 1 / scale_itr->get<double>();
            scale = Matrix34d::scale({scale_num, scale_num, scale_num});
        }
    }

    auto rotation_itr = json.find("rotation");
    auto rotation = Matrix34d::identity();
    if (rotation_itr != json.end() && rotation_itr->is_object()) {
        auto axis = Vec3d{0, 0, 0};
        double angle = 0;
        auto x_itr = rotation_itr->find("x");
        if (x_itr != rotation_itr->end()) { axis.x() = x_itr->get<double>(); }
        auto y_itr = rotation_itr->find("y");
        if (y_itr != rotation_itr->end()) { axis.y() = y_itr->get<double>(); }
        auto z_itr = rotation_itr->find("z");
        if (z_itr != rotation_itr->end()) { axis.z() = z_itr->get<double>(); }
        auto degrees_itr = rotation_itr->find("degrees");
        if (degrees_itr != rotation_itr->end()) { angle = -pi_d * degrees_itr->get<double>() / 180.0; }

        if (axis.length() > 0) { rotation = Matrix34d::rotate(axis.normalize(), angle); }
    }

    return scale * rotation * translation;
}
