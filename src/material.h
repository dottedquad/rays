#ifndef MATERIAL_H_
#define MATERIAL_H_

#include "vec.h"

struct RGB {
    float r;
    float g;
    float b;

public:
    RGB(const float& rd, const float& gr, const float& bl) : r{rd}, g{gr}, b{bl} {}
    RGB(const Vec3f& vec) : r{vec._v[0]}, g{vec._v[1]}, b{vec._v[2]} {}
};

struct Material {
    RGB ambient;
    RGB diffuse;
    RGB specular;

public:
    Material(const RGB& a, const RGB& d, const RGB& s) : ambient{a}, diffuse{d}, specular{s} {}
};

#endif
