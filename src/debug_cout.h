#ifndef DEBUG_COUT_H_
#define DEBUG_COUT_H_

//#include "scene.h"
#include "material.h"
#include "matrix.h"
#include "ray.h"
#include "vec.h"
#include <ostream>


inline std::ostream& operator<<(std::ostream& os, const RGB& rgb)
{
    return os << '(' << rgb.r << ',' << rgb.g << ',' << rgb.b << ')';
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Vec3<T>& v)
{
    return os << '(' << v._v[0] << ',' << v._v[1] << ',' << v._v[2] << ')';
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Vec4<T>& v)
{
    return os << '(' << v._v[0] << ',' << v._v[1] << ',' << v._v[2] << ',' << v._v[3] << ')';
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix33<T>& m)
{
    return os << '(' << Vec3<T>{m._m[0]} << ',' << Vec3<T>{m._m[1]} << ',' << Vec3<T>{m._m[2]} << ')';
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Matrix34<T>& m)
{
    return os << '(' << Vec4<T>{m._m[0]} << ',' << Vec4<T>{m._m[1]} << ',' << Vec4<T>{m._m[2]} << ')';
}

template <typename T>
std::ostream& operator<<(std::ostream& os, const Ray<T>& r)
{
    return os << '(' << r._origin << ")->(" << r._direction << ')';
}

// inline std::ostream& operator<<(std::ostream& os, const Intersection& i) {
// 	os << "t: " << i.t << " n: " << i.normal;
// }

// inline std::ostream& operator<<(std::ostream& os, const std::vector<Intersection>& is) {
// 	os << "intersections " << is.size() << "\n";
// 	for (const auto& i: is) {
// 		os << i << "\n";
// 	}	
// }

#endif