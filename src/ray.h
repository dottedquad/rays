#ifndef RAY_H_
#define RAY_H_

#include "matrix.h"
#include "vec.h"

template <typename T>
class Ray {
public:
    Ray(const Vec3<T>& origin, const Vec3<T>& direction) : _origin{origin}, _direction{direction.normalize()}, _inv_direction{T{1}/direction.normalize()}
    {
    }

    const Vec3<T> direction() const { return _direction; }
    const Vec3<T> inv_direction() const { return _inv_direction; }
    const Vec3<T> origin() const { return _origin; }
    const Vec3<T> destination(const T& t) const { return _origin + t * _direction; }

    // Ray<T> transform(const Matrix34<T>& transform, const Matrix34<T>& inv_transform) const {
    // 	return Ray<T>{
    // 		inv_transform * origin(),
    // 		inv_transform.normal_transform(direction())
    // 	};
    // }

    Vec3<T> _origin;
    Vec3<T> _direction;
    Vec3<T> _inv_direction;
};

template<typename T>
Ray<T> operator*(const Matrix34<T>& m, const Ray<T>& r)
{
	Ray<T> n{m * r._origin, m.normal_transform(r._direction)};
	return n;

	//return {m * r._origin, m*r._direction};
}

using RayD = Ray<double>;
using RayLd = Ray<long double>;
using RayF = Ray<float>;

#endif
