#ifndef BOX_H_
#define BOX_H_

#include "matrix.h"
#include "ray.h"
#include "vec.h"
#include "debug_cout.h"

#include <cmath>
#include <iostream>
#include <optional>

class Box {
public:
    Box(const Matrix34d& obj_to_world, const Matrix34d& world_to_obj) : _obj_to_world{obj_to_world}, _world_to_obj{world_to_obj}
    {
    }

    std::optional<std::pair<double, Vec3d>> intersect(const RayD& r) const
    {
        //Vec3d ns[3][2] = {{{1,0,0}, {-1,0,0}},{{0,1,0}, {0,-1,0}},{{0,0,1}, {0,0,-1}}};

        // const double tx1 = (b.min.x - r.x0.x)*r.n_inv.x;
        // const double tx2 = (b.max.x - r.x0.x)*r.n_inv.x;
        const auto ray = _world_to_obj * r;

        // std::cout << "r " << r << '\n';
        // std::cout << "_world_to_obj " << _world_to_obj << '\n';
        // std::cout << "_obj_to_world " << _obj_to_world << '\n';
        // std::cout << "ray " << ray << '\n';

        const double tx1 = (-1 - ray.origin().x())*ray.inv_direction().x();
        const double tx2 = (1 - ray.origin().x())*ray.inv_direction().x();

        //std::cout << "tx1 " << tx1 << '\n';
        //std::cout << "tx2 " << tx2 << '\n';

        double tmin = std::min(tx1, tx2);
        double tmax = std::max(tx1, tx2);
     
        //std::cout << "tmin " << tmin << '\n';
        //std::cout << "tmax " << tmax << '\n';

        const double ty1 = (-1 - ray.origin().y())*ray.inv_direction().y();
        const double ty2 = (1 - ray.origin().y())*ray.inv_direction().y();
     
        //std::cout << "ty1 " << tx1 << '\n';
        //std::cout << "ty2 " << tx2 << '\n';

        tmin = std::max(tmin, std::min(ty1, ty2));
        tmax = std::min(tmax, std::max(ty1, ty2));

        //std::cout << "tmin " << tmin << '\n';
        //std::cout << "tmax " << tmax << '\n';

        const double tz1 = (-1 - ray.origin().z())*ray.inv_direction().z();
        const double tz2 = (1 - ray.origin().z())*ray.inv_direction().z();
     
        //std::cout << "tz1 " << tx1 << '\n';
        //std::cout << "tz2 " << tx2 << '\n';

        tmin = std::max(tmin, std::min(tz1, tz2));
        tmax = std::min(tmax, std::max(tz1, tz2));

        //std::cout << "tmin " << tmin << '\n';
        //std::cout << "tmax " << tmax << '\n';

        if (tmax >= tmin && tmin > 0) {
            // std::cout << "hit! "<<tmin<<"\n";

            const auto pt = ray.destination(tmin);
            double max_val = pt.x();
            double abs_max_val = std::abs(pt.x());
            auto n = Vec3d{1,0,0};

            if (std::abs(pt.y()) > abs_max_val) {
                abs_max_val = std::abs(pt.y());
                max_val = pt.y();
                n = Vec3d{0,1,0};
            }
            if (std::abs(pt.z()) > abs_max_val) {
                abs_max_val = std::abs(pt.z());
                max_val = pt.z();
                n = Vec3d{0,0,1};
            }
            if (max_val < 0) {
                n = -n;
            }

            const auto p = _obj_to_world * pt;
            // std::cout << "o p" << pt << "\n";
            // std::cout << "o n" << n << "\n";
            // std::cout << "world p" << p << "\n";
            // std::cout << "world n" << _world_to_obj.normal_transposed_transform(n).normalize() << "\n\n";

            return std::make_pair(r.direction().dot(p-r.origin()),
                _world_to_obj.normal_transposed_transform(n).normalize());
        } 
        else {
            return {};
        }
    }

private:
    Matrix34d _obj_to_world;
    Matrix34d _world_to_obj;

};

#endif