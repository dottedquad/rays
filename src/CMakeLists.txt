project(rays)

add_executable(rays
	rays.cpp
	object.cpp
	scene.cpp

	box.h
	debug_cout.h
	image.h
	material.h
	matrix.h
	object.h
	ray.h
	scene.h
	sphere.h
	triangle.h
	tristrip.h
	vec.h)

if(MSVC)
  target_compile_options(rays PRIVATE /W4 /WX)
else()
  target_compile_options(rays PRIVATE -Wall -Wextra -pedantic -Werror)
endif()

set(THREADS_PREFER_PTHREAD_FLAG ON)
find_package(Threads REQUIRED)
target_link_libraries(rays Threads::Threads)