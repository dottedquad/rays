#ifndef SPHERE_H_
#define SPHERE_H_

#include "matrix.h"
#include "ray.h"
#include "vec.h"
#include <vector>

//#include "debug_cout.h"
//#include <iostream>

// template <typename T>
// std::ostream& operator<<(std::ostream& os, const Vec3<T>& v)
// {
//     return os << '(' << v._v[0] << ',' << v._v[1] << ',' << v._v[2] << ')';
// }


template <typename T>
class Sphere {
public:
    //Sphere(const Vec3<T>& center, const T& radius) : _center{center}, _radius{radius} {}
    Sphere(const Matrix34<T>& obj_to_world, const Matrix34<T>& world_to_obj) : _obj_to_world{obj_to_world}, _world_to_obj{world_to_obj} {}

    //const Vec3<T>& center() const { return _center; }
    //const T& radius() const { return _radius; }

public:
    // std::vector<std::pair<T, Vec3<T>>> intersect(const Ray<T>& r) const
    // {
    //     // https://en.wikipedia.org/wiki/Line–sphere_intersection

    //     const auto oc = r._origin - _center;
    //     const auto loc = r._direction.dot(oc);
    //     const auto oc2 = oc.dot(oc);
    //     const auto r2 = _radius * _radius;
    //     const auto under_radical = loc * loc - oc2 + r2;
    //     if (under_radical < 0.0) { return {}; }
    //     const auto radical = std::sqrt(under_radical);
    //     const auto d1 = -loc + radical;
    //     const auto d2 = -loc - radical;
    //     const auto p1 = r.destination(-loc - radical);
    //     const auto p2 = r.destination(-loc + radical);
    //     const auto n1 = (p1 - _center).normalize();
    //     const auto n2 = (p2 - _center).normalize();
    //     return {std::make_pair(-loc - radical, n1), std::make_pair(-loc + radical, n2)};
    // }
    std::vector<std::pair<T, Vec3<T>>> intersect(const Ray<T>& r) const
    {
        // https://en.wikipedia.org/wiki/Line–sphere_intersection

        const auto ray = _world_to_obj * r;

        const auto oc = ray._origin;
        const auto loc = ray._direction.dot(oc);
        const auto oc2 = oc.dot(oc);
        const auto r2 = 1;//_radius * _radius;
        const auto under_radical = loc * loc - oc2 + r2;
        if (under_radical < 0.0) { return {}; }
        const auto radical = std::sqrt(under_radical);
        const auto d1 = -loc + radical;
        const auto d2 = -loc - radical;
        // std::cout << "d1 " << d1 << "\n";
        // std::cout << "d2 " << d2 << "\n";
        const Vec3d l_p1 = ray.destination(d2);
        const Vec3d l_p2 = ray.destination(d1);
        // std::cout << "lp1 " << l_p1 << "\n";
        // std::cout << "lp2 " << l_p2 << "\n";
        const auto p1 = _obj_to_world * l_p1;
        const auto p2 = _obj_to_world * l_p2;
        // std::cout << "p1 " << p1 << "\n";
        // std::cout << "p2 " << p2 << "\n";
        const auto n1 = _world_to_obj.normal_transposed_transform(l_p1).normalize();
        const auto n2 = _world_to_obj.normal_transposed_transform(l_p2).normalize();
        // std::cout << "n1 " << n1 << "\n";
        // std::cout << "n2 " << n2 << "\n";
        return {std::make_pair(r.direction().dot(p1-r.origin()), n1), std::make_pair(r.direction().dot(p2-r.origin()), n2)};
    }

    const Matrix34<T>& obj_to_world() const { return _obj_to_world; }
    const Matrix34<T>& world_to_obj() const { return _world_to_obj; }

private:
    //Vec3<T> _center;
    //T _radius;
    Matrix34<T> _obj_to_world;
    Matrix34<T> _world_to_obj;
};

using SphereD = Sphere<double>;
using SphereLd = Sphere<long double>;
using SphereF = Sphere<float>;

#endif
