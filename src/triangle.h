#ifndef TRIANGLE_H_
#define TRIANGLE_H_

#include "matrix.h"
#include "ray.h"
#include "vec.h"

#include <cmath>
#include <iostream>
#include <optional>

class Triangle {
public:
    Triangle(const Vec3d& a, const Vec3d& b, const Vec3d& c,
        const Matrix34d& obj_to_world, const Matrix34d& world_to_obj) : _a{a}, _b{b}, _c{c}, _ab{b-a}, _ac{c-a}, _n{(c-a).cross(b-a).normalize()}, _obj_to_world{obj_to_world}, _world_to_obj{world_to_obj}
    {
    }

    std::optional<std::pair<double, Vec3d>> intersect(const RayD& r) const
    {
        const auto ray = _world_to_obj * r;

        const double EPSILON = 0.0000001;

        const auto h = ray.direction().cross(_ac);
        const auto a = _ab.dot(h);
        if (a > -EPSILON && a < EPSILON) {
            return {}; // This ray is parallel to this triangle.
        }
        const auto f = 1.0 / a;
        const auto s = ray.origin() - _a;
        const auto u = f * s.dot(h);
        if (u < 0.0 || u > 1.0) { return {}; }
        const auto q = s.cross(_ab);
        const auto v = f * ray.direction().dot(q);
        if (v < 0.0 || u + v > 1.0) { return {}; }
        // At this stage we can compute t to find out where the intersection point
        // is on the line.
        float t = f * _ac.dot(q);
        if (t > EPSILON && t < 1 / EPSILON) // ray intersection
        {
            const auto p = _obj_to_world * ray.destination(t);
            return std::make_pair(r.direction().dot(p-r.origin()),
                _world_to_obj.normal_transposed_transform(_n).normalize());
        }
        else { // This means that there is a line intersection but not a ray
               // intersection.
            return {};
        }
    }

    const Vec3d& a() const { return _a; }
    const Vec3d& b() const { return _b; }
    const Vec3d& c() const { return _c; }

private:
    Vec3d _a;
    Vec3d _b;
    Vec3d _c;
    Vec3d _ab;
    Vec3d _ac;
    Vec3d _n;

    Matrix34d _obj_to_world;
    Matrix34d _world_to_obj;

};

#endif