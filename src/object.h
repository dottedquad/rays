#ifndef OBJECT_H_
#define OBJECT_H_

#include "matrix.h"
#include "sphere.h"
#include <memory>
#include <nlohmann/json.hpp>
#include <vector>

class Object;
class Scene;

class Object {
public:
    Object();
    Object(const Matrix34d& parent_obj_to_world, const Matrix34d& parent_world_to_obj, const nlohmann::json& json);

    void add_child(const nlohmann::json& json);

    const std::vector<SphereD>& get_spheres() const { return _spheres; }

private:
    Matrix34d _obj_to_world;
    Matrix34d _world_to_obj;

    std::vector<Object> _children;

    std::vector<SphereD> _spheres;

};

#endif
