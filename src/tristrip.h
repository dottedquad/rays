#ifndef TRISTRIP_H_
#define TRISTRIP_H_

#include "matrix.h"
#include "ray.h"
#include "vec.h"

#include <cmath>
#include <iostream>
#include <vector>

class TriStrip {
public:
    TriStrip(const std::vector<Vec3d>& vertices, const Matrix34d& obj_to_world, const Matrix34d& world_to_obj) : _vertices{vertices}, _obj_to_world{obj_to_world}, _world_to_obj{world_to_obj}
    {
        if (vertices.size() < 3) {
            throw std::runtime_error("tristrip too few vertices");
        }

        for (size_t tri_idx=0; tri_idx<size(); ++tri_idx) {
            const auto ab = b(tri_idx) - a(tri_idx);
            const auto ac = c(tri_idx) - a(tri_idx);
            _abs.push_back(ab);
            _acs.push_back(ac);
            _ns.push_back({ac.cross(ab).normalize()});
        }
    }

    const Vec3d& a(size_t idx) const {
        return (idx&1) ? _vertices[idx+1] : _vertices[idx+0];
    }

    const Vec3d& b(size_t idx) const {
        return (idx&1) ? _vertices[idx+0] : _vertices[idx+1];
    }

    const Vec3d& c(size_t idx) const {
        return _vertices[idx+2];
    }

    size_t size() const {
        return _vertices.size() - 2;
    }

    std::vector<std::pair<double, Vec3d>> intersect(const RayD& r) const
    {
        const auto ray = _world_to_obj * r;

        std::vector<std::pair<double, Vec3d>> intersections;

        for (size_t tri_idx=0; tri_idx<size(); ++tri_idx) {
            if (auto intersection = tri_intersect(ray, r, tri_idx)) {
                intersections.push_back(*intersection);
            }
        }

        return intersections;
    }

    std::optional<std::pair<double, Vec3d>> tri_intersect(const RayD& ray, const RayD& r, size_t idx) const
    {
        const auto& a = this->a(idx);
        const auto& ab = _abs[idx];
        const auto& ac = _acs[idx];
        const auto& n = _ns[idx];

        const double EPSILON = 0.0000001;

        const auto h = ray.direction().cross(ac);
        const auto ab_dot_h = ab.dot(h);
        if (ab_dot_h > -EPSILON && ab_dot_h < EPSILON) {
            return {}; // This ray is parallel to this triangle.
        }
        const auto f = 1.0 / ab_dot_h;
        const auto s = ray.origin() - a;
        const auto u = f * s.dot(h);
        if (u < 0.0 || u > 1.0) { return {}; }
        const auto q = s.cross(ab);
        const auto v = f * ray.direction().dot(q);
        if (v < 0.0 || u + v > 1.0) { return {}; }
        // At this stage we can compute t to find out where the intersection point
        // is on the line.
        float t = f * ac.dot(q);
        if (t > EPSILON && t < 1 / EPSILON) // ray intersection
        {
            const auto p = _obj_to_world * ray.destination(t);
            return std::make_pair(r.direction().dot(p-r.origin()),
                _world_to_obj.normal_transposed_transform(n).normalize());
        }
        else { // This means that there is a line intersection but not a ray
               // intersection.
            return {};
        }
    }

private:
    std::vector<Vec3d> _vertices;
    std::vector<Vec3d> _abs;
    std::vector<Vec3d> _acs;
    std::vector<Vec3d> _ns;

    Matrix34d _obj_to_world;
    Matrix34d _world_to_obj;
};

#endif