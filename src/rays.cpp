#include "camera.h"
#include "debug_cout.h"
#include "image.h"
#include "matrix.h"
#include "ray.h"
#include "scene.h"
#include "sphere.h"
#include "triangle.h"
#include "vec.h"
#include <algorithm>
#include <cassert>
#include <cmath>
#include <fstream>
#include <iostream>
#include <mutex>
#include <random>
#include <thread>
#include <vector>

std::vector<std::mt19937> gens;
std::vector<std::mt19937> gens2;
std::vector<std::uniform_real_distribution<>> dist;
std::vector<std::uniform_real_distribution<>> dist2;

int main()
{
    const int width = 500;
    const int height = 500;
    const int samples = 100000;

    auto image = Image{width, height};

    const double v_angle = pi_d / 3;
    const double h_angle = pi_d / 3;

    std::string contents;
    std::ifstream file("../scene/john-scene.json");
    if (file) {
        file.seekg(0, std::ios::end);
        size_t len = file.tellg();
        file.seekg(0);
        contents = std::string(len + 1, '\0');
        file.read(&contents[0], len);
    }

    const auto scene = Scene{contents};

    const auto viewpoint = Vec3d{0, 0, 5};

    const auto camera = Camera{viewpoint, {0, 0, 0}, v_angle, h_angle};

    //std::random_device rd;

    size_t nthreads = std::thread::hardware_concurrency();
    //unsigned int nthreads = 1;
    size_t lines_per_chunk = (height/nthreads)/16;
    lines_per_chunk = std::max(lines_per_chunk,size_t(1));

    std::vector<size_t> master_lines;
    for (size_t y = 0; y < height; y++) {
        master_lines.push_back(y);
    }
    std::cout << "master_lines has num: " << master_lines.size() << "\n";
    std::mutex m;


    std::vector<std::thread> threads;
    threads.reserve(nthreads);

    //gens.resize(nthreads);
    //gens2.resize(nthreads);
    //dist.resize(nthreads);
    //dist2.resize(nthreads);
    gens.reserve(nthreads);
    gens2.reserve(nthreads);
    dist.reserve(nthreads);
    dist2.reserve(nthreads);

    for (size_t thread_id = 0; thread_id<nthreads; ++thread_id) {
        std::cout << "starting thread: " << thread_id << "\n";
        gens.emplace_back(thread_id);
        dist.emplace_back(0.0, 1.0);
        gens2.emplace_back(thread_id+10000);
        dist2.emplace_back(0.0, 1.0);

        threads.emplace_back([&camera, &scene, &image, &master_lines, &m, thread_id, lines_per_chunk]() {
            auto& gen = gens[thread_id];
            auto& dis = dist[thread_id];
            auto& gen2 = gens2[thread_id];
            auto& dis2 = dist2[thread_id];

            std::vector<size_t> lines;
            size_t total = 0;
            while (true) {
                lines.clear();
                {
                    const std::lock_guard<std::mutex> lock{m};
                    for (size_t i=0; i<lines_per_chunk; ++i) {
                        if (!master_lines.empty()) {
                            lines.emplace_back(master_lines.back());
                            master_lines.pop_back();
                        }
                        else {
                            break;
                        }
                    }
                }
                if (!lines.empty()) {
                total += lines.size();
                std::cout << "thread " << thread_id<< "grabbed " << lines.size() << " lines\n";
                }
                else if (lines.empty()) {
                    std::cout << "exiting after rendering " << total << " total lines\n";
                    return;
                }
                for (const auto& line_num: lines) {
                    for (size_t x = 0; x < width; x++) {
                        for (size_t i=0; i<samples; ++i) {
                            const auto x_rand = dis(gen);
                            const auto y_rand = dis(gen);
                            const double y_d = (y_rand + line_num) / (height - 1);
                            const double x_d = (x_rand + x) / (width - 1);

                            const auto ray = camera.get_ray(x_d, y_d);

                            std::vector<Intersection> ds = scene.intersect(ray);
                            std::sort(ds.begin(), ds.end());

                            const auto color = scene.shade(ds, ray, gen2, dis2);
                            image.pixel(x, line_num) += {color.r, color.g, color.b};
                        }
                        image.pixel(x, line_num) /= samples;
                    }
                }
                std::cout << thread_id << " rendered " << lines.size() << " lines, going back for more\n";

            }
        });
    }
    for (auto& thread: threads) {
        thread.join();
    }


    // for (size_t y = 0; y < height; y++) {
    //     for (size_t x = 0; x < width; x++) {
    //         for (size_t i=0; i<samples; ++i) {
    //         	const auto x_rand = dis(gen);
    //         	const auto y_rand = dis(gen);
		  //       const double y_d = (y_rand + y) / (height - 1);
    //     	    const double x_d = (x_rand + x) / (width - 1);

    //         	const auto ray = camera.get_ray(x_d, y_d);

    //         	std::vector<Intersection> ds = scene.intersect(ray);
    //         	std::sort(ds.begin(), ds.end());

    //         	const auto color = scene.shade(ds, ray);
    //         	image.pixel(x, y) += {color.r, color.g, color.b};
    //         }
	   //      image.pixel(x, y) /= samples;
    //     }
    // }

    std::ofstream fout("john.pfm", std::ios::out);
    fout << image;

    fout.close();
}